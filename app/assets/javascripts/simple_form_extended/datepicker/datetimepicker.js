$(document).on('ready page:change nested:fieldAdded', function() {
  $('.datetimepicker').each(function(index, el) {
    var el = $(el)
    return el.datetimepicker({
      icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-arrow-up',
        down: 'fa fa-arrow-down',
        next: 'fa fa-arrow-right',
        previous: 'fa fa-arrow-left'
      }
    })
  })

  $('.datetimerange').each(function(index, el) {
    var $this = $(el)
    var range1 = $($this.find('.input-group')[0])
    var range2 = $($this.find('.input-group')[1])
    if (range1.data('DateTimePicker').date() !== null) {
      range2.data('DateTimePicker').minDate(range1.data('DateTimePicker').date())
    }
    if (range2.data('DateTimePicker').date() !== null) {
      range1.data('DateTimePicker').maxDate(range2.data('DateTimePicker').date())
    }
    range1.on('dp.change', function(e) {
      if (e.date) {
        range2.data('DateTimePicker').minDate(e.date)
      } else {
        range2.data('DateTimePicker').minDate(false)
      }
    });
    range2.on('dp.change', function(e) {
      if (e.date) {
        return range1.data('DateTimePicker').maxDate(e.date)
      } else {
        return range1.data('DateTimePicker').maxDate(false)
      }
    })
  })
})
