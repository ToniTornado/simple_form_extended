// http://momentjs.com/
//= require moment
//= require moment/de

// https://github.com/zpaulovics/datetimepicker-rails
//= require bootstrap-datetimepicker
//= require simple_form_extended/datepicker/datetimepicker
