// https://github.com/selectize/selectize.js
//= require jquery-ui/widgets/sortable
//= require simple_form_extended/select/selectize
//= require simple_form_extended/select/drag_drop
//= require simple_form_extended/select/multiselect
//= require simple_form_extended/select/remoteselect
