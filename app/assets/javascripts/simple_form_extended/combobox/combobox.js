$(document).on('ready page:change nested:fieldAdded', function() {
  $('.combobox-field').each(function(index, el) {
    el = $(el)
    return el.combobox({newOptionsAllowed: el.attr("data-new-options-allowed")==='1'})
  })
})
