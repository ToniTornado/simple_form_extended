$(document).on('ready page:change nested:fieldAdded', function() {
  $('.color-picker-field').each(function(index, el) {
    el = $(el)
    return el.ColorPickerSliders({
      placement: 'top',
      preventtouchkeyboardonshow: true,
      previewformat: 'hex',
      color: '#ffffff',
      swatches: el.attr('data-colors').split(','),
      customswatches: false,
      order: {}
    })
  })
})
