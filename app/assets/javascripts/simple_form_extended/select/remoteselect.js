$(document).on("ready page:change nested:fieldAdded", function() {
  $("select.remoteselect-field:not(.selectized)").each(function(index, el) {
    var el = $(el)
    var plugins = []
    if (el.attr("data-max-items") != "1") {
      plugins.push("remove_button")
    }
    if (el.attr("data-reorder") == "true") {
      plugins.push("drag_drop")
    }
    return el.selectize({
      selectOnTab: true,
      create: false,
      preload: el.attr("data-preload") == "true" ? true : false,
      maxItems: el.attr("data-max-items") == "unlimited" ? null : Number(el.attr("data-max-items")),
      plugins: plugins,
      dropdownParent: "body",
      inputClass: el.attr("data-max-items") == "unlimited" ? "selectize-input" : "form-control selectize-input",
      load: function(query, callback) {
        // Do not load anything if no data url is defined
        if (el.attr("data-url") === undefined || (el.attr("data-preload") != "true" && !query.length)) return callback()
        $.ajax({
          url: el.attr("data-url"),
          type: "GET",
          dataType: "json",
          data: {
            q: query
          },
          error: function() {
            callback()
          },
          success: function(res) {
            callback(res)
          }
        })
      }
    })
  })
})
