class ToggleInput < SimpleForm::Inputs::BooleanInput

  def input(wrapper_options = nil)
    set_input_options
    html_options = label_html_options.dup
    html_options[:class] ||= []
    html_options[:class].push(SimpleForm.boolean_label_class) if SimpleForm.boolean_label_class
    input_group
  end

  def label_input(wrapper_options = nil)
    set_input_options
    html_options = label_html_options.dup
    html_options[:class] ||= []
    html_options[:class].push(SimpleForm.boolean_label_class) if SimpleForm.boolean_label_class

    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)
    @builder.label(label_target, html_options) + input_group
  end

  def set_input_options
    input_options[:include_hidden] = true unless input_options[:include_hidden] == false
  end

  private

  def input_group
    template.content_tag :div, class: 'input-group' do
      input_group_addon +
      fake_input_field
    end
  end

  def input_group_addon
    template.content_tag :span, class: 'input-group-addon' do
      hidden_checkbox = input_options[:include_hidden] ? build_hidden_field_for_checkbox : ''.html_safe
      hidden_checkbox + build_check_box_without_hidden_field(input_html_options)
    end
  end

  def fake_input_field
    template.content_tag :div, class: 'form-control' do
      @builder.label(label_target, {class: 'toggle-inline-label'}) do
        " #{input_options[:description]}".html_safe
      end
    end
  end

end
