class ColorPickerInput < SimpleForm::Inputs::StringInput

  def input(wrapper_options)
    super(wrapper_options)
  end

  def input_html_classes
    super.push('color-picker-field')
  end

end
