class RemoteselectInput < SimpleForm::Inputs::CollectionSelectInput

  def input(wrapper_options)
    if options[:collection].present? && options[:collection].respond_to?(:-)
      options[:collection] -= [nil, '']
    end
    data = {}
    if options[:url].present?
      data[:url] = options[:url]
      if options[:url].present?
        parent_input_html_classes.push('selectize-search-field')
      end
    end
    if options[:preload] == true
      data['preload'] = 'true'
    end
    if options[:multiple] == true
      input_html_options[:multiple] = true
      data['max-items'] = 'unlimited'
    else
      data['max-items'] = '1'
    end
    if options[:reorder] == true
      data['reorder'] = 'true'
    end
    options[:label_method] ||= :label
    input_html_options[:data] = data
    super(wrapper_options)
  end

  alias_method :parent_input_html_classes, :input_html_classes
  def input_html_classes
    super.push('remoteselect-field')
  end

end
