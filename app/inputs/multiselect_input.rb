class MultiselectInput < SimpleForm::Inputs::CollectionSelectInput

  def input(wrapper_options)
    set_html_options
    super(wrapper_options)
  end

  def input_html_classes
    super.push('multiselect-field')
  end

  private

  def set_html_options
    input_html_options[:multiple] = true
  end

end
