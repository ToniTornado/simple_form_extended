class ComboboxInput < SimpleForm::Inputs::CollectionSelectInput

  def input(wrapper_options)
    super(wrapper_options)
  end

  def input_html_classes
    super.push('combobox-field form-control')
  end

end
