module SimpleFormExtended
  class Engine < ::Rails::Engine
    isolate_namespace SimpleFormExtended

    require 'simple_form'
    require 'jquery-ui-rails'
    require 'minimal_form_builder'
    require 'nested_form'
    # 10 MB und bisher nirgends benutzt?
    # require 'country_select'
    require 'momentjs-rails'
    require 'bootstrap3-datetimepicker-rails'

  end
end
