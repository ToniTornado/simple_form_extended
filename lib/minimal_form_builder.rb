# Zeigt anstatt des Labels den entsprechenden Placeholder an
class MinimalFormBuilder < SimpleForm::FormBuilder

  def input(attribute_name, options = {}, &block)
    i18n = {}
    if options[:placeholder].nil?
      options[:placeholder] ||= if object.respond_to? :translate
        object.translate(attribute_name, i18n.reverse_merge(include_associations: true))
      elsif object.class.respond_to?(:human_attribute_name)
        object.class.human_attribute_name(attribute_name.to_s)
      else
        attribute_name.to_s.humanize
      end
    end
    options[:label] = false if options[:label].nil?
    super
  end

end
