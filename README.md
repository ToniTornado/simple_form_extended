
# SimpleFormExtended Gem

This project contains the ruby gem **simple_form_extended**. This gem can be used
by Ruby-on-Rails applications which use simple form and want take advantage of
additional or improved input fields. You'll need this gem if you want to use
the ui_base gem.

# Installation

## Prerequisist
Your application should be based on Ruby-on-Rails > 5.0.0.

## Install Gem

* Chage the source in your ``Gemfile`` to a repository containing this gem.
We suggest to replace the source line by this block of code:
```
if ENV['GEMREPO']
    source ENV['GEMREPO']
else
    source 'http://nexus.it.gefa.de/nexus/content/repositories/rubygems'
end
```
You could then overwrite your source repository with your own by setting the enviroment
variable ``GEMREPO`` if you haven't got access to the GEFA gem repository.

* Add this Gem to your Gemfile: `gem 'simple_form_extended'`
* run `bundle install`
