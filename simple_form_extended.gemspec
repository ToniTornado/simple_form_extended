$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "simple_form_extended/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "simple_form_extended"
  s.version     = SimpleFormExtended::VERSION
  s.authors     = ["Robin Wielpuetz"]
  s.email       = ["tonitornado@gmail.com"]
  s.homepage    = "https://github.com/tonitornado/simple_form_extended"
  s.summary     = "Simple Form plus additional field types and forms."
  s.description = "Simple Form plus additional field types and forms."

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 5.0.0"
  s.add_dependency 'simple_form', "~> 3.4"
  s.add_dependency 'nested_form'
  s.add_dependency 'country_select'
  s.add_dependency 'jquery-ui-rails'


  # Bootstrap Datepicker
  # https://eonasdan.github.io/bootstrap-datetimepicker/Installing/#rails
  s.add_dependency  'momentjs-rails', '>= 2.9.0'
  s.add_dependency  'bootstrap3-datetimepicker-rails', '~> 4.14.30'
end
